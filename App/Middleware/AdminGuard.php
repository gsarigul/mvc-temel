<?php
    namespace App\Middleware;
    use Closure;

class AdminGuard {
    public function handle($request, $closure, $next, $guard = null){
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }
        return $next($request);
    }
}
?>