<?php
require(__DIR__ . '/../vendor/autoload.php');


use Illuminate\Cache\CacheManager;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Routing\Router;

// Hataları görebilmemiz lazım...
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/..');
$dotenv->load();

require(__DIR__ . '/functions.php');

//Cache'i ayarlıyoruz...
$container = new Container;
$eventTracker = new Dispatcher($container);
$container['config'] = [
    'cache.default' => 'file',
    'cache.stores.file' => [
        'driver' => 'file',
        'path' => __DIR__ . '/../onbellek/app'
    ]
];
$container['files'] = new Filesystem;
$cacheManager = new CacheManager($container);
$cache = $cacheManager->store();

// Veritabanını ayarlıyoruz
$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => env('DB_HOST'),
    'database' => env('DB_NAME'),
    'username' => env('DB_USER'),
    'password' => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
// Eloquent ile diğer event emitter aynı yere gelebilir mi?
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Router'i ayarlıyoruz

$globalMiddleware = [
    // \App\Middleware\StartSession::class
];
$routerMiddleware = [
    'auth' => \App\Middleware\AuthGuard::class,
    'admin' => \App\Middleware\AdminGuard::class,
];

$request = Request::capture();
$container->instance('Illuminate\Http\Request', $request);

$router = new Router($eventTracker, $container);

foreach ($routerMiddleware as $key => $middleware) {
    $router->aliasMiddleware($key, $middleware);
}

require(__DIR__ . '/../router/routes.php');
$response = (new Pipeline($container))
    ->send($request)
    ->through($globalMiddleware)
    ->then(function ($request) use ($router) {
        return $router->dispatch($request);
    });

$response->send();
