<?php

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;

if(!function_exists('collect')){
    function collect($toCollect){
        return new Collection($toCollect);
    }
}
if(!function_exists('view')){
    function view($view, $templateData = null){
        $templatePath = [__DIR__.'/../views'];
        $pathToCompiled = __DIR__.'/../onbellek/views';
        $filesystem = new Filesystem;
        $eventEmitter = new Dispatcher(new Container);
        
        $viewResolver = new EngineResolver;
        $compiler = new BladeCompiler($filesystem, $pathToCompiled);
        $viewResolver->register('blade', function() use($compiler){
            return new CompilerEngine($compiler);
        });
        $viewResolver->register('php', function() use($filesystem){
            return new PhpEngine($filesystem);
        });

        $viewFinder = new FileViewFinder($filesystem, $templatePath);
        $viewFactory = new Factory($viewResolver, $viewFinder, $eventEmitter);
        return $viewFactory->make($view)->with($templateData)->render();
    }
}

?>