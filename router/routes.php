<?php

use Illuminate\Routing\Router;
use Illuminate\Http\RedirectResponse;

$router->get('/', function () {
    return 'hello world!';
});

$router->get('bye', function () {
    return 'goodbye world!';
});
$router->get('view-test', 'App\Controllers\HomeController@Index');
